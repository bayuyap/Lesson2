package id.sch.smktelkom_mlg.lesson2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText searchbox;

    private TextView udisp;

    private TextView result;


    private TextView error;


    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchbox = findViewById(R.id.search);

        udisp = findViewById(R.id.display);
        result = findViewById(R.id.search_result);


        error = findViewById(R.id.tv_error_message_display);


        loading = findViewById(R.id.pb_loading_indicator);
    }


    private void makeGithubSearchQuery() {
        String githubQuery = searchbox.getText().toString();
        URL githubSearchUrl = NetworkUtils.buildUrl(githubQuery);
        udisp.setText(githubSearchUrl.toString());
        new GithubQueryTask().execute(githubSearchUrl);
    }

    private void showJsonDataView() {

        error.setVisibility(View.INVISIBLE);

        result.setVisibility(View.VISIBLE);
    }


    private void showErrorMessage() {

        result.setVisibility(View.INVISIBLE);

        error.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();
        if (itemThatWasClickedId == R.id.action_search) {
            makeGithubSearchQuery();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class GithubQueryTask extends AsyncTask<URL, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(URL... params) {
            URL searchUrl = params[0];
            String githubSearchResults = null;
            try {
                githubSearchResults = NetworkUtils.getResponseFromHttpUrl(searchUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return githubSearchResults;
        }

        @Override
        protected void onPostExecute(String githubSearchResults) {

            loading.setVisibility(View.INVISIBLE);
            if (githubSearchResults != null && !githubSearchResults.equals("")) {

                showJsonDataView();
                result.setText(githubSearchResults);
            } else {

                showErrorMessage();
            }
        }
    }
}
